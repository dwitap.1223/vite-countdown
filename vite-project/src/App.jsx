import { useState, useEffect } from "react";
import "./App.css";
import mountain from "./assets/pattern-hills.svg";
import star from "./assets/bg-stars.svg";
import facebook from "./assets/icon-facebook.svg";
import pinterest from "./assets/icon-pinterest.svg";
import instagram from "./assets/icon-instagram.svg";

function App() {
  const calculateTimeLeft = () => {
    const difference = +new Date("2024-07-17") - +new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => clearTimeout(timer);
  });

  return (
    <div className="main">
      <div className="sub-container">
        <div className="title">WE'RE LAUNCHING SOON</div>
        <div className="content">
          <div className="item">
            <div className="time">{timeLeft.days}</div>
            <div className="text">DAYS</div>
          </div>
          <div className="item">
            <div className="time">{timeLeft.hours}</div>
            <div className="text">HOURS</div>
          </div>
          <div className="item">
            <div className="time">{timeLeft.minutes}</div>
            <div className="text">MINUTES</div>
          </div>
          <div className="item">
            <div className="time">{timeLeft.seconds}</div>
            <div className="text">SECONDS</div>
          </div>
        </div>
        <div className="image-container">
          <div>
            <img src={facebook} alt="" />
          </div>
          <div>
            <img src={pinterest} alt="" />
          </div>
          <div>
            <img src={instagram} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
